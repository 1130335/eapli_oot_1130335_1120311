package Aplicacional;

import Domínio.FamíliaFibra;
import Domínio.PedidoOrçamento;
import Domínio.TipoCor;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class OrçamentoControllerTest {
    
    public OrçamentoControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testCalculaOrçamento_Qt50_CorClara() {
        System.out.println("Calcula Orçamento");
        FamíliaFibra familia = new FamíliaFibra("PA", "Poliamida", 0.85);
        TipoCor tipo = new TipoCor("Clara", 0.80);
        double quantidade = 50;
        PedidoOrçamento po = new PedidoOrçamento(familia, tipo, quantidade);
        OrçamentoController instance = new OrçamentoController();
        double expResult = 150;
        instance.calculaOrçamento(po);
        // assertEquals(expResult, result, 0.0);

    }
    
}
