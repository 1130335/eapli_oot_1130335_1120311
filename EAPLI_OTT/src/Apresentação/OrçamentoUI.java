package Apresentação;

import Aplicacional.OrçamentoController;
import Domínio.FamíliaFibra;
import Domínio.PedidoOrçamento;
import Domínio.TipoCor;
import Persistência.RepositórioFamíliaFibra;
import Persistência.RepositórioTiposCor;
import java.util.List;
import java.util.Scanner;

public class OrçamentoUI {
    
    private OrçamentoController oController;
    private RepositórioFamíliaFibra rep_família;
    private RepositórioTiposCor rep_tipo;
    private PedidoOrçamento pedido;
    
    public OrçamentoUI() {
        oController = new OrçamentoController();
        rep_família = new RepositórioFamíliaFibra();
        rep_tipo = new RepositórioTiposCor();
    }
    
    public void mostra() {
        System.out.println("Família de Fibras disponíveis: ");
        List<FamíliaFibra> lista_família = rep_família.todas();
        for(int i = 0; i<4; i++) {
            System.out.println(lista_família.get(i));
        }
        System.out.println("\nIndique em que posição se encontra a família que pretende (1 - 4): ");
        Scanner in = new Scanner(System.in);
        int item;
        item = Integer.parseInt(in.nextLine());
        FamíliaFibra família = lista_família.get(item-1);
        
        System.out.println("\nTipos de cor disponíveis:");
        List<TipoCor> lista_tipo = rep_tipo.todas();
        for(int j = 0; j<3; j++) {
            System.out.println(lista_tipo.get(j));
        }
        System.out.println("\nIndique em que posição se encontra o tipo de cor que pretende (1 - 3): ");
        item = Integer.parseInt(in.nextLine());
        TipoCor tipo = lista_tipo.get(item-1);
        
        System.out.println("\nIndique a quantidade (em Kg): ");
        double quantidade;
        quantidade = Double.parseDouble(in.nextLine());
        
        pedido = new PedidoOrçamento(família, tipo, quantidade);
        
        oController.calculaOrçamento(pedido);
        
        System.out.println("\nFibra: " + família);
        System.out.println("\nQuantidade: " + quantidade+ " Kg");
        System.out.println("\nTipo de Cor: " + tipo + "\n");
        
        System.out.println("\nOrçamento " + oController.getOrçamento() + " euros");
        System.out.println("\n    Matéria-Prima: " + oController.getMatériaPrima() + " euros");
        System.out.println("\n    Produção: " + oController.getProdução() + " euros");
        System.out.println("\n    Margem: " + oController.getMargem() + " euros");
    }
    
}