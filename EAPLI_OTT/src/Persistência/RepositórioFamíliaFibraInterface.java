package Persistência;

import Domínio.FamíliaFibra;
import java.util.List;

public interface RepositórioFamíliaFibraInterface {
    
    List<FamíliaFibra> todas();
    
}
