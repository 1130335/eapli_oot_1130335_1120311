package Persistência;

import Domínio.FamíliaFibra;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RepositórioFamíliaFibra implements RepositórioFamíliaFibraInterface{

    private static final List<FamíliaFibra> dados = new ArrayList<>();
    
    public RepositórioFamíliaFibra() {
        // Código
    }
    
    static {
        dados.add(new FamíliaFibra("PA", "Poliamida", 0.85));
        dados.add(new FamíliaFibra("PES", "Poliester", 0.80));
        dados.add(new FamíliaFibra("SP", "Spandex", 0.75));
        dados.add(new FamíliaFibra("VI", "Viscose", 0.70));
    }
    
    @Override
    public List<FamíliaFibra> todas() {
        return Collections.unmodifiableList(dados);
    }
    
}
