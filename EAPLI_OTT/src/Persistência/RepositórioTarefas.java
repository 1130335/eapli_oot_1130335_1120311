package Persistência;

import Domínio.Tarefa;
import Domínio.TarefaTextil;
import Domínio.TarefaTinturaria;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RepositórioTarefas implements RepositórioTarefasInterface{
    
    private static final List<Tarefa> dados = new ArrayList<>();
    
    public RepositórioTarefas() {
        // Código
    }
    
    static {
        dados.add(new TarefaTextil("Extrusão", 0.30));
        dados.add(new TarefaTextil("Texturização", 0.50));
        dados.add(new TarefaTextil("Torcedura", 0.25));
        dados.add(new TarefaTinturaria("Tinturaria"));
        dados.add(new TarefaTextil("Bobinagem", 0.20));
        dados.add(new TarefaTextil("Revista", 0.10));
    }

    @Override
    public List<Tarefa> todas() {
        return Collections.unmodifiableList(dados);
    }
}
