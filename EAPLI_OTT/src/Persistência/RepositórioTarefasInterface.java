
package Persistência;

import Domínio.Tarefa;
import java.util.List;

public interface RepositórioTarefasInterface {
    
    List<Tarefa> todas();
    
}
