package Persistência;

import Domínio.TipoCor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RepositórioTiposCor implements RepositórioTiposCorInterface{

    private static final List<TipoCor> dados = new ArrayList<>();
    
    public RepositórioTiposCor() {
        // Código
    }
    
    static {
        dados.add(new TipoCor("Clara", 0.80));
        dados.add(new TipoCor("Escura", 1.10));
        dados.add(new TipoCor("Intensa", 1.35));
    }
    
    @Override
    public List<TipoCor> todas() {
        return Collections.unmodifiableList(dados);
    }
    
}
