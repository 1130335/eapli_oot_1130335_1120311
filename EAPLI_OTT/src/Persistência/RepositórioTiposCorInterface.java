package Persistência;

import Domínio.TipoCor;
import java.util.List;

public interface RepositórioTiposCorInterface {
   
    List<TipoCor> todas();
}
