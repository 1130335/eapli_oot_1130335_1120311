package Domínio;

import java.util.List;

public class ProcessoProdutivo {
   
    public ProcessoProdutivo() {
    
    }
    
    public double custo(PedidoOrçamento pedido,List<Tarefa> dados) {
            double custo=0;
           
            for(Tarefa d: dados){
                custo=d.preçoProdução(pedido);
            }
        return custo;
    }
    
}
