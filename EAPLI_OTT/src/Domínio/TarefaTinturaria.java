package Domínio;

public class TarefaTinturaria extends Tarefa {
    
    public TarefaTinturaria(String nome) {
        super(nome);
    }
    
    @Override
    public double preçoProdução(PedidoOrçamento pedido) {
        double preço_produção = 0;
        TipoCor tipo = pedido.getTipoCor();
        double quantidade = pedido.getQuantidade();
        preço_produção = tipo.getPreço()*quantidade;
        return preço_produção;
    }
}
