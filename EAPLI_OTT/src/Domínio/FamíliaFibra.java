package Domínio;

public class FamíliaFibra {
    
    private String nome;
    private String descrição;
    private double preço;
   
    public FamíliaFibra(String nome, String descrição, double preço) {
        this.nome=nome;
        this.descrição=descrição;
        this.preço=preço;
    }
    
    public double preçoMaterial(double quantidade) {
     
        return preço*quantidade;
    }
    
    public String toString(){
        return nome + "-" + descrição;
    }
    
}
