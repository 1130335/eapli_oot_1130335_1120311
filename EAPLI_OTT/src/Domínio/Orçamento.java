package Domínio;

import Persistência.RepositórioTarefas;
import java.util.List;

public class Orçamento {
    
    private PedidoOrçamento pedidoOrçamento;
    private RepositórioTarefas rt;
    public Orçamento(PedidoOrçamento pedido) {
        this.pedidoOrçamento=pedido;
        rt = new RepositórioTarefas();
    }
    
   
    
    public double custo() {
        
        return custoProduçãoPúblico()+custoMaterialPúblico()+margemPúblico();
    }
    
    public double custoMaterialPúblico() {
        
       return pedidoOrçamento.getFamília().preçoMaterial(pedidoOrçamento.getQuantidade());
    
    }
    
    public double custoProduçãoPúblico() {
        ProcessoProdutivo processo= new ProcessoProdutivo();
        
        return processo.custo(pedidoOrçamento, rt.todas());
        
    }
    
    public double margemPúblico() {
            
        return (custoProduçãoPúblico()+custoMaterialPúblico())*0.10;
    }
}
