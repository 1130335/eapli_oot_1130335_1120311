package Domínio;

public class TipoCor {
    
    private String descrição;
    private double preço;
    
    public TipoCor(String descrição, double preço) {
        this.descrição = descrição;
        this.preço = preço;
    }
    
    public double getPreço() {
        return preço;
    }
    
    public String toString() {
        return this.descrição;
    }
}
