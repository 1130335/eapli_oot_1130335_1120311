package Domínio;

public class PedidoOrçamento {
    
    private FamíliaFibra famíliaFibra;
    private TipoCor tipoCor;
    private double margem;
    private double quantidade;
    
    public PedidoOrçamento(FamíliaFibra famíliaFibra, TipoCor tipoCor, double quantidade) {
        this.famíliaFibra = famíliaFibra;
        this.tipoCor = tipoCor;
        this.quantidade = quantidade;
        
    }
    public TipoCor getTipoCor(){
        return this.tipoCor;
   }
    public double getQuantidade(){
        return this.quantidade;    
   }
    
    public FamíliaFibra getFamília(){
        return this.famíliaFibra;    
   }
}
