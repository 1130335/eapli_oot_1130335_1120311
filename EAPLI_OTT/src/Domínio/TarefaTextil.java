package Domínio;

import java.util.List;

public class TarefaTextil extends Tarefa {
    
    private double preço;
    
    public TarefaTextil(String nome, double preço) {
        super(nome);
        this.preço = preço;
    }
    
    @Override
    public double preçoProdução(PedidoOrçamento pedido) {
        double quantidade = pedido.getQuantidade();
        double preço_produção = 0;
        preço_produção = this.preço*quantidade;
        return 0;
    }
}
