package Aplicacional;

import Domínio.FamíliaFibra;
import Domínio.Orçamento;
import Domínio.PedidoOrçamento;
import Domínio.TipoCor;
import java.util.List;

public class OrçamentoController {
    
    private Orçamento orçamento;
    
    public OrçamentoController() {
        // Código
    }
    
    public void calculaOrçamento(PedidoOrçamento pedido) {
        orçamento = new Orçamento(pedido);
    }
}
